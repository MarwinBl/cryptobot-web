let video = document.getElementById("myVideo");

// Get the button
let play_btn = document.getElementById("myBtn");

// Pause and play the video, and change the button text
function myFunction() {
    if (video.paused) {
        video.play();
        play_btn.innerHTML = "Pause";
    } else {
        video.pause();
        play_btn.innerHTML = "Play";
    }
}