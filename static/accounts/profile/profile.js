//for start button

const start_bot_btn = document.getElementById('start-bot');
const cstf = document.getElementsByName('csrfmiddlewaretoken')[0].value;
const all_form = document.getElementsByTagName('input');
const stat_btn = document.getElementById('stat-bot');
const stat_box = document.getElementById('stat-box');

function getXmlHttp(){
    let xmlhttp;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest!=='undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

function change_status(btn){
    if (btn.getAttribute('status') === 'stopped'){
        btn.setAttribute('status', 'started');
        btn.setAttribute('class', 'btn btn-danger');
        btn.firstChild.data = 'Остановить';
        stat_btn.style.display = 'inline-block'
    }else {
        btn.setAttribute('status', 'stopped');
        btn.setAttribute('class', 'btn btn-success');
        btn.firstChild.data = 'Запустить';
        clearInterval(stat_interval);
        stat_btn.style.display = 'none'
    }
}

function show_eror(id_error_block, text){
    text = text || '';
    id_error_block = id_error_block || 'startBotError';
    error_block = document.getElementById(id_error_block);
    error_block.innerHTML = text;
    if (text === 'CryptoDaemon already started' || text === 'ERR: Can not find your worker process by id'){
        change_status(start_bot_btn)
    }
}

start_bot_btn.onclick = function(){
    XmlHttp = getXmlHttp();
    XmlHttp.open('POST', 'start');
    XmlHttp.setRequestHeader("X-CSRFToken", cstf);
    XmlHttp.setRequestHeader("X-Requested-With", 'XMLHttpRequest');
    XmlHttp.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
    XmlHttp.onreadystatechange = function() {
        if(XmlHttp.readyState === 4) {
            if(XmlHttp.status === 200) {
                let resp = JSON.parse(XmlHttp.responseText);
                if (resp.status === 'OK'){
                    change_status(start_bot_btn);
                    show_eror()
                }else if(resp.status === 'error'){
                    show_eror(null, resp.message)
                }
            }
        }
    };

    let btn_status = start_bot_btn.getAttribute('status');
    let form_data = {};
    let exchange_choose = document.getElementById('exchange_name');

    form_data['exchange'] = exchange_choose.options[exchange_choose.selectedIndex].text;

    if(btn_status==='stopped'){
        form_data['action'] = 'start'
    }else {
        form_data['action'] = 'stop'
    }

    for (let i = 0; i < all_form.length; i++) {
        if (all_form[i].name === 'csrfmiddlewaretoken') continue;
        form_data[all_form[i].name] = all_form[i].value;
    }

    XmlHttp.send(JSON.stringify(form_data));
};



// temp stat btn

var stat_interval = null;

stat_btn.onclick = function(){
    status_start_btn = start_bot_btn.getAttribute('status');
    status_stat_btn = stat_btn.getAttribute('status');
    if (status_start_btn === 'started' && status_stat_btn === 'stopped'){
        stat_btn.setAttribute('status', 'started');
        stat_btn.className = 'btn btn-info';
        stat_interval = setInterval(function(){
            get_stat()
        }, 2000);
    } else {
        stat_btn.setAttribute('status', 'stopped');
        stat_btn.className = 'btn btn-outline-info';
        clearInterval(stat_interval)
    }

};


function get_stat(update=false) {
    XmlHttp = getXmlHttp();
    XmlHttp.open('POST', 'stat');
    XmlHttp.setRequestHeader("X-CSRFToken", cstf);
    XmlHttp.setRequestHeader("X-Requested-With", 'XMLHttpRequest');
    XmlHttp.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
    XmlHttp.onreadystatechange = function() {
        if(XmlHttp.readyState === 4) {
            if(XmlHttp.status === 200) {
                let resp = JSON.parse(XmlHttp.responseText);
                if (resp){
                    console.log(resp);
                    for (obj in resp){
                        let div = '<div class="card" date="' + resp[obj].date + '">' +
                            '    <div class="card-header" id="heading' + resp[obj].date + '">' +
                            '      <h5 class="mb-0">' +
                            '        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse' + resp[obj].date + '" aria-expanded="false" aria-controls="collapse' + resp[obj].date + '">' +
                                      resp[obj].msg +
                            '        </button>' +
                                    '<small class="badge badge-dark small float-right">' + get_date_str(resp[obj].date) + '</small>' +
                        '      </h5>' +
                            '    </div>' +
                            '    <div id="collapse' + resp[obj].date + '" class="collapse" aria-labelledby="heading' + resp[obj].date + '" data-parent="#stat-box">' +
                            '      <div class="card-body">' +
                                    resp[obj].ext_msg +
                            '      </div>' +
                            '    </div>' +
                            '  </div>';
                        stat_box.insertAdjacentHTML('afterbegin', div)
                    }
                    $('button').on('click', function () {
                        $(this).parents('.card').find('.collapse').collapse('toggle')
                    })
                }
            }
        }
    };
    if (stat_box.childElementCount !== 0){
        let last_upd = stat_box.firstChild.getAttribute('date');
        req = {'update': true, 'time': last_upd}
    } else {
        req = {'update': false}
    }

    XmlHttp.send(JSON.stringify(req));

}

function get_date_str(timestamp) {
    sec = parseInt(timestamp);
    date = new Date(timestamp*1000);
    let options = {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        hour:  'numeric',
        minute:  'numeric',
        second:  'numeric',
        timezone: 'Europe/Moscow'
    };
    return date.toLocaleString('ru', options)
}

//
//
// //for stat button
//
// const stat_socket = new WebSocket(
//     'ws://' + window.location.host +
//     '/ws/bot_stat/'
//     );
//
// stat_socket.onmessage = function (e) {
//     let data = JSON.parse(e.data);
//     let message = data.message;
//     stat_box.insertAdjacentHTML('afterbegin', '<div class="alert alert-primary">' + message + '</div>')
// };
//
// stat_socket.onclose = function (e) {
//     console.error('socket is close');
// };
//
// stat_btn.onclick = function (e) {
//     stat_socket.send(JSON.stringify({
//         'message': 'start-stat'
//     }))
// };