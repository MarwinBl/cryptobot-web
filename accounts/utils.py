import json
import requests
from django.conf import settings
from .models import ExchangeKey


SUCCESS_START = 'CryptoDaemon started successfully'
SUCCESS_STOP = 'CryptoDaemon instance stopped'
ALREADY_START = 'CryptoDaemon already started'
NOT_FIND_BOT = 'ERR: Can not find your worker process by id'

OK = {'status': 'OK'}
NOT_AVAILABLE = {'status': 'error',
                 'message': 'Бот не доступен'}
BOT_ERROR = {'status': 'error',
             'message': ''}


def build_request(request_body, user_id):
    req_dict = get_json(request_body)
    action = req_dict.pop('action')
    public_key = req_dict.pop('public_key')
    secret_key = req_dict.pop('secret_key')
    exchange = req_dict.pop('exchange')
    return {
        'id': user_id,
        'action': action,
        'api_keys': {
            'public_key': public_key,
            'secret_key': secret_key,
            'exchange': exchange.lower()
        },
        'strategy-settings': parse_settings(req_dict),
    }


def get_json(body):
    return json.loads(body)


def parse_settings(settings):
    return {
        'purchase_different_coins': int(settings['purchase_different_coins']),
        'drop_range_to_buy_pct': [int(settings['drop_range_to_buy_pct_min']), int(settings['drop_range_to_buy_pct_max'])],
        'deposit_threshold_pct': int(settings['deposit_threshold_pct']),
        'capitalization_threshold_usd': int(settings['capitalization_threshold_usd']),
        'market_volume_threshold_usd_24h': int(settings['market_volume_threshold_usd_24h']),
        'your_margin_pct': int(settings['your_margin_pct']),
        'crypto_only': True if settings['crypto_only'] == 'on' else False,
    }


def request_to_bot(data):
    try:
        resp = requests.post(f'http://{settings.DEFAULT_API_HOST}:{settings.DEFAULT_API_PORT}', json=data)
    except:
        resp = None
    return resp


def action_bot(action, user):
    if action == SUCCESS_START:
        ExchangeKey().change_satus_bot(user)
        return OK
    elif action == SUCCESS_STOP:
        ExchangeKey().change_satus_bot(user, False)
        return OK
    elif action == ALREADY_START:
        ExchangeKey().change_satus_bot(user, comment=action)
        BOT_ERROR['message'] = action
        return BOT_ERROR
    elif action == NOT_FIND_BOT:
        ExchangeKey().change_satus_bot(user, False, NOT_FIND_BOT)
        BOT_ERROR['message'] = action
        return BOT_ERROR
    else:
        BOT_ERROR['message'] = action
        return BOT_ERROR


def resp_processing(response, user):
    return NOT_AVAILABLE if response is None else action_bot(response.text, user)
