from django.contrib.auth.views import LoginView
from django.contrib.auth import logout, login
from django.shortcuts import redirect
from django.views.generic import CreateView
from django.http import HttpResponseForbidden, JsonResponse

from .utils import *
from accounts.form import AuthenticateForm, RegistrationForm, ProfileForm
from bot_api.models import BotStat


class RegistrationView(CreateView):
    form_class = RegistrationForm
    template_name = 'accounts/registration.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseForbidden()
        return super(RegistrationView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        user = form.save(commit=False)
        user.set_password(form.cleaned_data['password_2'])
        user.save()
        login(self.request, user)
        return redirect('profile')


class AuthenticateView(LoginView):
    form_class = AuthenticateForm
    template_name = 'accounts/authenticate.html'


class ProfileView(CreateView):
    form_class = ProfileForm
    template_name = 'accounts/profile.html'

    def dispatch(self, request, *args, **kwargs):
        print(dir(self.form_class.Meta.model.created))
        if request.method == 'POST' and self._is_api_form(request):
            exchange = ExchangeKey()
            exchange.save_from_request(request)
        if request.user.is_authenticated:
            return super(ProfileView, self).dispatch(request, *args, **kwargs)
        return HttpResponseForbidden()

    def _is_api_form(self, request):
        required_fields = {'public_key': 32, 'secret_key': 32, 'csrfmiddlewaretoken': 64, 'exchange_name': 3}
        request_fields = request._post
        if len(request_fields.keys()) == len(required_fields):
            for key, value in required_fields.items():
                try:
                    if len(request_fields[key]) != value:
                        return False
                except KeyError:
                    return False
            else:
                return True

    def form_valid(self, form):
        strategy = form.save(commit=False)
        strategy.user = self.request.user
        strategy.save()
        return redirect('profile')


class LogoutView(CreateView):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            logout(request)
            return redirect('authenticate')
        return HttpResponseForbidden()


def start_bot(request):
    if request.is_ajax():
        body = build_request(request.body, request.user.id)
        resp = request_to_bot(body)
        return JsonResponse(resp_processing(resp, request.user))


def stat_bot(request):
    if request.is_ajax():
        json_req = get_json(request.body)
        if json_req['update']:
            stat = [{'msg': obj.stat_message, 'ext_msg': obj.ext_stat_message,
                     'date': obj.created.timestamp()} for obj in BotStat.objects.filter(user=request.user).order_by('created')\
                    if obj.created.timestamp() > float(json_req['time'])]
        else:
            stat = [{'msg': obj.stat_message, 'ext_msg': obj.ext_stat_message,
                     'date': obj.created.timestamp()} for obj in BotStat.objects.filter(user=request.user).order_by('-created')[:5]]
            stat.reverse()
        return JsonResponse(stat, safe=False)