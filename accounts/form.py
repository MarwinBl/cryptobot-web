from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from .models import Strategy


class AuthenticateForm(AuthenticationForm):
	username = forms.CharField(widget=forms.TextInput(attrs={
		'placeholder': 'Логин',
		'class': 'form-control'}))

	password = forms.CharField(widget=forms.PasswordInput(attrs={
		'placeholder': 'Пароль',
		'class': 'form-control'}))


class RegistrationForm(forms.ModelForm):
	username = forms.CharField(max_length=32, strip=True, widget=forms.TextInput(attrs={
		'placeholder': 'Логин',
		'class': 'form-control'}))

	email = forms.EmailField(max_length=64, widget=forms.EmailInput(attrs={
		'placeholder': 'Почта',
		'class': 'form-control'}))

	password_1 = forms.CharField(max_length=128, strip=True, widget=forms.PasswordInput(attrs={
		'placeholder': 'Пароль',
		'class': 'form-control'}))

	password_2 = forms.CharField(max_length=128, strip=True, widget=forms.PasswordInput(attrs={
		'placeholder': 'Повторите пароль',
		'class': 'form-control'}))

	class Meta:

		model = User
		fields = ['username', 'email']

	def clean_email(self):
		email = self.cleaned_data['email']
		if User.objects.filter(email=email).exists():
			raise forms.ValidationError('Данный email уже зарегистрирован')
		return email

	def clean_password_2(self):
		clear_data = self.cleaned_data
		if clear_data['password_1'] != clear_data['password_2']:
			raise forms.ValidationError('Пароли не совпадают')
		return clear_data['password_2']


class ProfileForm(forms.ModelForm):

	class Meta:
		model = Strategy
		exclude = ['user']