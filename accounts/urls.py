from django.conf.urls import url
from django.contrib.auth import logout
from .views import *


urlpatterns = [
	url('authenticate/', view=AuthenticateView.as_view(), name='authenticate'),
	url('registration/', view=RegistrationView.as_view(), name='registration'),
	url('profile/$', view=ProfileView.as_view(), name='profile'),
	url('logout/', view=LogoutView.as_view(), name='logout'),
	url('profile/start', view=start_bot, name='start_bot'),
	url('profile/stat', view=stat_bot, name='stat_bot'),
]