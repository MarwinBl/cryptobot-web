from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import User


class Strategy(models.Model):

	user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name='user strategy', primary_key=True)
	purchase_different_coins = models.PositiveSmallIntegerField(default=5)
	drop_range_to_buy_pct_min = models.PositiveSmallIntegerField(default=10)
	drop_range_to_buy_pct_max = models.PositiveSmallIntegerField(default=40)
	deposit_threshold_pct = models.PositiveSmallIntegerField(default=50)
	capitalization_threshold_usd = models.IntegerField(default=50000000)
	market_volume_threshold_usd_24h = models.IntegerField(default=0, blank=True, null=True)
	your_margin_pct = models.PositiveSmallIntegerField(default=10)
	crypto_only = models.BooleanField(default=False)
	created = models.DateTimeField(auto_now_add=True, null=True)
	updated = models.DateTimeField(auto_now=True)

	def __str__(self):
		return f'{self.user.username} strategy'


class ExchangeKey(models.Model):

	HITBTC = 'HIT'
	POLONIEX = 'PLN'
	EXMO = 'EXM'
	EXCHANGES = (
		(HITBTC, 'HitBTC'),
		(POLONIEX, 'Poloniex'),
		(EXMO, 'Exmo')
	)
	user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name='user api keys', primary_key=True)
	public_key = models.CharField(max_length=32, blank=True)
	secret_key = models.CharField(max_length=32, blank=True)
	exchange_name = models.CharField(max_length=3, choices=EXCHANGES, default=HITBTC)
	is_started = models.BooleanField(default=False)
	comment = models.CharField(max_length=256, null=True, blank=True)

	def __str__(self):
		return f'{self.user.username} key'

	def save_from_request(self, request):
		dict_req = request._post
		self.user = request.user
		self.secret_key = dict_req['secret_key']
		self.public_key = dict_req['public_key']
		self.exchange_name = dict_req['exchange_name']
		self.save(force_update=True)

	def change_satus_bot(self, user, start=True, comment=''):
		exc = ExchangeKey.objects.get(user=user)
		exc.is_started = start
		exc.comment = comment
		exc.save()


def strategy_post_save(sender, instance, created, **kwargs):
	if Strategy.objects.filter(user=instance).exists():
		pass
	else:
		Strategy.objects.create(user=instance)


def hitbtc_post_save(sender, instance, created, **kwargs):
	if ExchangeKey.objects.filter(user=instance).exists():
		pass
	else:
		ExchangeKey.objects.create(user=instance)


post_save.connect(strategy_post_save, sender=User)
post_save.connect(hitbtc_post_save, sender=User)