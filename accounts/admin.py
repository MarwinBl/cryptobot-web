from django.contrib import admin
from .models import *


class StrategyAdmin(admin.ModelAdmin):

	list_display = [field.name for field in Strategy._meta.fields]

	class Meta:
		model = Strategy


class ExchangeKeyAdmin(admin.ModelAdmin):

	class Meta:
		model = ExchangeKey


admin.site.register(Strategy, StrategyAdmin)
admin.site.register(ExchangeKey, ExchangeKeyAdmin)
