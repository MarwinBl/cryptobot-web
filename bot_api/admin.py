from django.contrib import admin
from .models import BotStat


class BotStatAdmin(admin.ModelAdmin):

    class Meta:
        model = BotStat


admin.site.register(BotStat, BotStatAdmin)