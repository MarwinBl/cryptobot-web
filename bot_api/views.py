import json
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import BotStat


@csrf_exempt
def stat(request):
    req_json = json.loads(request.body)
    id = req_json.get('id')
    stat_message = req_json.get('stat_message')
    ext_stat_message = req_json.get('ext_stat_message')
    bs = BotStat()
    bs.save_by_id(id, stat_message, ext_stat_message)
    return HttpResponse(status=200)
