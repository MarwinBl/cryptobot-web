from django.db import models
from django.contrib.auth.models import User


class BotStat(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    stat_message = models.CharField(max_length=256)
    ext_stat_message = models.TextField(default=None, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    def save_by_id(self, id, stat_message, ext_stat_message=None):
        _user = User.objects.get(id=id)
        if _user:
            _bs = BotStat()
            _bs.user = _user
            _bs.stat_message = stat_message
            _bs.ext_stat_message = ext_stat_message
            _bs.save(force_insert=True)
